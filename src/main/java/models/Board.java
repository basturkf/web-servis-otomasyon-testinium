package models;


public class Board {

    private String id;
    private String name;
    private String descData;
    private boolean closed;
    private String idOrganization;
    private String idEnterprise;
    private boolean pinned;
    private String url;


    public Board(String id, String name, String descData, boolean closed, String idOrganization, String idEnterprise, boolean pinned, String url) {
        this.id = id;
        this.name = name;
        this.descData = descData;
        this.closed = closed;
        this.idOrganization = idOrganization;
        this.idEnterprise = idEnterprise;
        this.pinned = pinned;
        this.url = url;
    }

    public Board() {
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescData() {
        return descData;
    }

    public boolean isClosed() {
        return closed;
    }

    public String getIdOrganization() {
        return idOrganization;
    }

    public String getIdEnterprise() {
        return idEnterprise;
    }

    public boolean isPinned() {
        return pinned;
    }

    public String getUrl() {
        return url;
    }

    public String toString() {
        return String.format("Board data: id: %s name: %s", getId(), getName());
    }

}