package data;

public enum  DataManager {
    API_KEY("Trello_Api_Key"),
    //https://trello.com/1/authorize?expiration=never&scope=read,write,account&response_type=token&name=ServerToken&key=
    TOKEN("Trello_Api_Token"),
    CARD_NUMBER("2");

    private final String data;

    DataManager(String data)
    {
        this.data = data;
    }

    public String getData()
    {
        return data;
    }

    public int getDataInt()
    {
        try {
            return Integer.parseInt(data);
        }catch (Exception e){
            return -1;
        }
    }

}
