package utils;

public class AutomationProcess {

    public static void WebTest_Automation_Task_Start() {
        LogUtil.info("1- Trello's web test scenario begins.");
    }

    public static void WebTest_Automation_Task_TestStart() {
        LogUtil.info("2- Starting test scenarios in Trello Tests");
    }

    public static void WebTest_Automation_Task_Test(int index, String scenarioName) {
        LogUtil.info(" \t2." + index + "- " + scenarioName + " test scenario in Trello Tests");
    }

    public static void WebTest_Automation_Task_Create_Page(String scenarioName) {
        LogUtil.info(" \t\t" + scenarioName + " test started for Trello Tests");
    }

    public static void WebTest_Automation_Task_Finish_Page(String scenarioName) {
        LogUtil.info(" \t\t" + scenarioName + " test completed for Trello Tests\n");
    }

    public static void WebTest_Automation_Task_Finish_Browser(long testTime, String driverName) {
        LogUtil.info("3- Test Complete Time:" + testTime + ", Browser Name: " + driverName + " is closed\n\n");
    }

}
