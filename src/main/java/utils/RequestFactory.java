package utils;

import io.restassured.specification.RequestSpecification;
import models.Board;
import models.Card;

import java.util.ArrayList;

public class RequestFactory {
    private static final ThreadLocal<ArrayList<Board>> boardList = new ThreadLocal<>();
    private static final ThreadLocal<ArrayList<Card>> cardList = new ThreadLocal<>();
    private static final ThreadLocal<Long> timeThreadLocal = new ThreadLocal<>();

    public static synchronized void addBoard(Board id) {
        if (boardList.get() == null) {
            ArrayList<Board> temp = new ArrayList<>();
            boardList.set(temp);
        }
        boardList.get().add(id);
    }

    public static synchronized Board getBoard(int index) {
        return boardList.get().get(index);
    }

    public static synchronized ArrayList<Board> getBoardList() {
        return boardList.get();
    }

    public static synchronized void addCard(Card id) {
        if (cardList.get() == null) {
            ArrayList<Card> temp = new ArrayList<>();
            cardList.set(temp);
        }
        cardList.get().add(id);
    }

    public static synchronized Card getCard(int index) {
        return cardList.get().get(index);
    }

    public static synchronized ArrayList<Card> getCardList() {
        return cardList.get();
    }

    public static synchronized void setTime(long date){
        timeThreadLocal.set(date);
    }

    public static synchronized long getTime(){
        return timeThreadLocal.get();
    }
}
