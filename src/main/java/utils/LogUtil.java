package utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LogUtil {
    private static final Logger logger = LogManager.getLogger(LogUtil.class.getName());

    public static void info(String message) {
        logger.info(message);
    }

    public static void otherInfo(String message) {
        logger.info(" \t\t\t\t --->"+message);
    }

    public static void warn(String message) {
        logger.warn(message);
    }

    public static void error(String message,Object errorMessage) {
        logger.error("Error: "+message+ "\n\t Error Message:"+errorMessage);
    }

    public static void fatal(String message) {
        logger.fatal(message);
    }

    public static void debug(String message) {
        logger.debug(message);
    }
}
