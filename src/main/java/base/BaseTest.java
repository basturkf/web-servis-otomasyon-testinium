package base;

import io.restassured.RestAssured;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import utils.RequestFactory;

import static utils.AutomationProcess.*;

public class BaseTest {

    @BeforeClass
    public static void setUp() {
        RestAssured.baseURI = "https://api.trello.com/1";
        RequestFactory.setTime(System.currentTimeMillis());
        WebTest_Automation_Task_Start();
        WebTest_Automation_Task_TestStart();
    }

    @AfterClass
    public static void tearDown() {
        long testTime = System.currentTimeMillis() - RequestFactory.getTime();
        WebTest_Automation_Task_Finish_Browser(testTime, "Chrome");
    }
}
