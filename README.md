# Web Service Automation Task

Bu projede Testinium tarafından verilen task oluşturulmuş olup senaryoların Object Oriented Programming kullanılarak koşulması sağlanmıştır. Test projesinin başlatılması için `src\test\java\Trello.java` dosyasını JUnit ile çalıştırılmalıdır. Bu projede kullanılanlar:
- Java
- Maven
- Rest-Asured
- Junit
- Log4J


## Proje Özellikleri
* Web servisi testi çalıştırılabilmesi için trello api key ve token oluşturulmalıdır. Trelloda oturum açıldıktan sonra oluşturulan api key ile `https://trello.com/1/authorize?expiration=never&scope=read,write,account&response_type=token&name=ServerToken&key={YOUR_API_KEY}` adresinden token alınmalıdır.
* Web servis testi sırasında gerekli olan parametreler(Api key, Token) `src\main\java\data\DataManager.java` altında Enum içerikleri düzenlenmeldir.

            API_KEY("Trello_Api_Key"),
            TOKEN("Trello_Api_Token"),
            CARD_NUMBER("2");
        
* Projenin log kaydı tutulmaktadır. Log kaydı error, info ve benzeri başlıklar için ayrı ayrı tutulmaktadır. Log kayıtları `Logs/` altında zamana göre tutulmaktadır.

## Testinium Task İsterleri

- Trello üzerinde bir board oluşturunuz.
- Oluşturduğunuz board’ a iki tane kart oluşturunuz.
- Oluştrduğunuz bu iki karttan random olacak sekilde bir tanesini güncelleyiniz.
- Oluşturduğunuz kartları siliniz.
- Oluşturduğunuz board’ u siliniz.

## Test Log Çıktısı

         1- Trello's web test scenario begins.
         2- Starting test scenarios in Trello Tests
                2.1- Create Board test scenario in Trello Tests
                        Create Board test started for Trello Tests
                        Create Board test completed for Trello Tests

                2.2- Create Card test scenario in Trello Tests
                        Create Card test started for Trello Tests
                        Create Card test completed for Trello Tests

                2.3- Update To Card test scenario in Trello Tests
                        Update To Card test started for Trello Tests
                        Update To Card test completed for Trello Tests

                2.4- Remove Card test scenario in Trello Tests
                        Remove Card test started for Trello Tests
                        Remove Card test completed for Trello Tests

                2.5- Remove Board test scenario in Trello Tests
                        Remove Board test started for Trello Tests
                        Remove Board test completed for Trello Tests

         3- Test Complete Time:13097, Browser Name: Chrome is closed






